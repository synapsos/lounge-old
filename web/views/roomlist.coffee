define((require) ->
  $ = require('jquery')
  Backbone = require('backbone')
  Marionette = require('marionette')
  RoomModel = require('models/room')
  RoomView = require('views/room')

  RoomCollection = Backbone.Collection.extend({
    model: RoomModel
    url: '/r/'
  })
## TODO: CompositeView
  return Marionette.CollectionView.extend({
    itemView: RoomView
    template: require('templates/roomlist')
    collection: new RoomCollection()
    initialize: () ->
      @.collection.fetch()
  })
)
