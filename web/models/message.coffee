define((require) ->
  
  Backbone = require('backbone')

  return Backbone.RelationalModel.extend({
    urlRoot: '/m/'
    idAttribute: '_id'
  })
)
