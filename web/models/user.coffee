define((require) ->
  
  Backbone = require('backbone')

  return Backbone.RelationalModel.extend({
    urlRoot: '/u/',
    idAttribute: '_id',
    relations:
      type: Backbone.HasMany
      key: 'messages'
      relatedModel: '$.lounge.Message'
      reverseRelation:
        key: 'author'
        includeInJSON: '_id'
  })
)
