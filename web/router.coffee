define((require) ->
  Marionette = require('marionette')
  TodoListView = require('views/todolist')
  RoomView = require('views/room')
  RoomListView = require('views/roomlist')
  UserView = require('views/user')
  UserListView = require('views/userlist')
  MessageView = require('views/message')
  
  Router = Marionette.AppRouter.extend({
    routes: {
      '': 'index'
      '/r/:id': 'room'
      '/r': 'roomlist'
      '/u/:id': 'user'
      '/u': 'userlist'
      '/m/:id': 'message'
      
    }
    app = require('app')
    index: () ->
      app.content.show(new TodoListView())
    room: () ->
      app.content.show(new RoomView())
    roomlist: () ->
      app.content.show(new RoomListView())
    user: () ->
      app.content.show(new UserView())
    userlist: () ->
      app.content.show(new UserListView())
    message: () ->
      app.content.show(new MessageView())
  })

  return new Router()
)
