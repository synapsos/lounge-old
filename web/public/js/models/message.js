(function() {
  define(function(require) {
    var Backbone;

    Backbone = require('backbone');
    return Backbone.RelationalModel.extend({
      urlRoot: '/m/',
      idAttribute: '_id'
    });
  });

}).call(this);
