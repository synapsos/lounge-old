(function() {
  define(function(require) {
    var Backbone;

    Backbone = require('backbone');
    return Backbone.RelationalModel.extend({
      urlRoot: '/u/',
      idAttribute: '_id',
      relations: {
        type: Backbone.HasMany,
        key: 'messages',
        relatedModel: '$.lounge.Message',
        reverseRelation: {
          key: 'user',
          includeInJSON: '_id'
        }
      }
    });
  });

}).call(this);
