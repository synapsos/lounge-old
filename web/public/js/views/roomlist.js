(function() {
  define(function(require) {
    var $, Backbone, Marionette, RoomCollection, RoomModel, RoomView;

    $ = require('jquery');
    Backbone = require('backbone');
    Marionette = require('marionette');
    RoomModel = require('models/room');
    RoomView = require('views/room');
    RoomCollection = Backbone.Collection.extend({
      model: RoomModel,
      url: '/r/'
    });
    return Marionette.CollectionView.extend({
      itemView: RoomView,
      template: require('templates/roomlist'),
      collection: new RoomCollection(),
      initialize: function() {
        return this.collection.fetch();
      }
    });
  });

}).call(this);
