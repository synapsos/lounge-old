mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId
#MessageSchema = mongoose.model('Message').schema
#RoomSchema = mongoose.model('Room').schema

UserSchema = new Schema({
  id: ObjectId
  username: String
  email: String
  #messages: [MessageSchema]
  #rooms: [RoomSchema]
})

module.exports = mongoose.model('User', UserSchema)
