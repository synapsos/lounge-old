mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId
#UserSchema = mongoose.model('User').schema
#MessageSchema = mongoose.model('Message').schema

RoomSchema = new Schema({
  id: ObjectId
  title: String
  #users: [UserSchema]
  #messages: [MessageSchema]
})

module.exports = mongoose.model('Room', RoomSchema)
