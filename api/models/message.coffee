mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId
#UserSchema = mongoose.model('User').schema

MessageSchema = new Schema({
  id: ObjectId
  date_added: Date
  #author: [UserSchema]
  text: String
})

module.exports = mongoose.model('Message', MessageSchema)
