mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId

UserSchema = new Schema()
RoomSchema = new Schema()
MessageSchema = new Schema()

UserSchema.add({
  id: ObjectId
  username: String
  email: String
  messages: [MessageSchema]
  rooms: [RoomSchema]
})

RoomSchema.add({
  id: ObjectId
  title: String
  users: [UserSchema]
  messages: [MessageSchema]
})

MessageSchema.add({
  id: ObjectId
  date_added: Date
  author: [UserSchema]
  text: String
})

module.exports = {
  User: mongoose.model('User', UserSchema)
  Room: mongoose.model('Room', RoomSchema)
  Message: mongoose.model('Message', MessageSchema)
}
