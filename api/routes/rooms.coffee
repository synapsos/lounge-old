
Room = require('../models/room')

# List rooms
exports.list = (req, res) ->
  res.json([{title: 'First room'}])

exports.post = (req, res) ->
  room = new Room({title: 'test'})
  room.save((err) ->
    if err? then next(err)
    res.json(room)
  )

exports.put = (req, res) ->
  res.send(404, 'Implement')
